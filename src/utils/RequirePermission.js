import React from "react";
import { connect } from "react-redux";
import {push} from "connected-react-router";

//only admin has permission
export default function requirePermission(Component) {
    class PermissionComponent extends React.Component {
        constructor(props) {
            super(props);
            this.checkPerm();
        }

        componentDidUpdate(prevProps, prevState) {
            this.checkPerm();
        }

        //check if user has permission
        //only allowed if expert or admin
        checkPerm() {
            if (localStorage.getItem("group")!== "expert"&&localStorage.getItem("group")!== "admin") {
               //redirect to map if no permission
                this.props.dispatch(push(`/map`));
            }
        }

        render() {
            return (
                <div>
                    {localStorage.getItem("group")=== "expert"||localStorage.getItem("group")=== "admin"? (
                        <Component {...this.props} />
                    ) : null}
                </div>
            );
        }
    }


    return connect()(PermissionComponent);
}