import React, {Component} from 'react';
import './Nav.css';
import style from "../Button.css";
import {Link, withRouter} from "react-router-dom";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {logout} from "../Login/LoginActions";
import {push} from "connected-react-router";
class Nav extends Component{


    state = {clicked:false}


    handleClick = () =>{
        this.setState({clicked: !this.state.clicked})
    }
    componentDidMount() {

        const group= localStorage.getItem('group')
        this.setState({ group: group})
    }

    onLogout = async () => {
        window.localStorage.clear();
        this.setState({isLoggedIn: false})
        await localStorage.setItem("username", "");
        await localStorage.setItem("group", "");
        this.props.logout();


    };
    handleLog = () =>{
        if(localStorage.getItem('token')!=undefined){
            this.onLogout()
        }
    }

    render(){
        let { user } = this.props.auth;
        user = user.username==undefined?"":user.username

        return(
            <nav className={"NavbarItems"}>
                <Link to="/home" style={{ textDecoration: 'none' }}><h1 className={" navbar-logo"}>BA CS{/*<i className={"fab fa-react"}></i>*/}</h1></Link>
                <div className={"menu-icon"} onClick={this.handleClick}>
                    <i className={this.state.clicked?'fas fa-times':'fas fa-bars'}></i>
                </div>
                <div className={this.state.clicked?'nav-menu active':'nav-menu'}>
                    {user!=""?<Link to="/home">
                        <button className={"btn"}>
                            Home
                        </button>
                    </Link>:""}
                    {user!=""?<Link to="/map">
                    <button className={"btn"}>
                        Karte
                    </button>
                    </Link>:""}
                    {user!=""&&(localStorage.getItem("group")=="admin"||localStorage.getItem("group")=="expert")?<Link to="/evaluation">
                        <button className={"btn"}>
                            Evaluierung
                        </button>
                    </Link>:""}

                        {user?  <button className={"btn"} onClick={this.handleLog}>
                            Abmelden: {user}
                        </button>:
                            <Link to={"/login"}><button className={"btn"}>
                            Login
                        </button></Link>}

                    {user!=""?"":<Link to="/signup">
                        <button className={"btn"}>
                            Konto erstellen
                        </button>
                    </Link>}
                </div>


            </nav>
        )
    }


}

//lets you create functions that dispatch when called, and pass those functions as props to your component.
const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(logout())
  }
}

const mapStateToProps = ({auth}) => {
  return {auth}
}

export default connect(mapStateToProps, mapDispatchToProps)(Nav)