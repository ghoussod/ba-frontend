import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {Link, withRouter} from "react-router-dom";
import styles from "./Home.css"
import {logout} from "./Login/LoginActions";
import axios from "axios";
import {toast} from "react-toastify";
import concept from "../concept.png"

class Home extends Component {

    //Handle logout: set username and group back to empty and do logout
    onLogout = () => {
        localStorage.setItem("username", "");
        localStorage.setItem("group", "");
        this.props.logout();


    };

    render() {
        return (
            <div className={"background"}>
                <div className={'textArea'}>
                    <h1>Bürgerbeteiligung in der Raumplanung</h1>
                    <img className={"image1"} src={concept}/>

                    <div className={"font2"}>Die Webseite soll die Beteiligung der Bürger*innen in der Raumplanung ermöglichen, indem jede*r seine Vorschläge zu neuen Standorten von diversen Dienstleistungen geben kann.
                    Die gesammelten Vorschläge werden dann mit einem Algorithmus ausgewertet, um den bestmöglichen Standort für die Dienstleistungen zu finden. <br/>
                        Um aktiv mitzumachen, melden Sie sich<Link to={"/login"} className={""}> hier</Link> an.<br/><br/>
                        {localStorage.getItem("user")!=""&&localStorage.getItem("user")!=undefined?<text className={"green"}>Klicken Sie auf "Karte" in der Navigation um ihre Vorschläge abzugeben.<br/><br/></text>:""}
                        {localStorage.getItem("group")=="expert"||localStorage.getItem("group")=="admin"?<text className={"blue"}>Klicken Sie auf "Evaluierung" in der Navigation um die ausgewerteten Daten zu finden.</text>:""}

                    </div>






                </div>
            </div>
        );
    }
}

//TODO: explain connect and mapStateToProps
//validate proptypes
Home.propTypes = {
    logout: PropTypes.func.isRequired, //logout function
    auth: PropTypes.object.isRequired //auth object
};

//If your mapStateToProps function is declared as taking one parameter,
// it will be called whenever the store state changes,
// and given the store state as the only parameter.
const mapStateToProps = state => ({
    auth: state.auth
});

// connect() returns a new connected component.
// In fact, connect() wraps your component in a new one, adding some useful features,
// that is why you usually export the return value of the connect function (the new component).
export default connect(mapStateToProps, {
    logout
})(withRouter(Home));

