import {OutTable, ExcelRenderer} from 'react-excel-renderer';
import {Component} from "react";
import axios from "axios";
import {toast} from "react-toastify";
class ExistingServices extends Component{

    state={
        rows:"",
        cols:"",
        allGroceryNames:"",
    }


    async componentDidMount() {

    }

    //TODO: deletion doesn't work: 405 method not allowed
    onClickDeleteAll=async () => {
        await axios
            .delete(`api/existingGrocery/`)
            .then((res) => {
                this.setState({allGroceryNames: ""})
            })
            .catch((error)=> {
                console.log(error)
            })
    }

    //send excel file to backend
    onClickSend=async () => {

        //get the list
        await axios
            .get(`api/existingGrocery/`, {
            })
            .then((res) => {
                let allGroceryNames =[]
                res.data.map(grocery => {
                            allGroceryNames.push(grocery.name)
                    }
                )
                this.setState({allGroceryNames: allGroceryNames})


            })
            .catch((error)=>{
                console.log(error)
            })

        if(this.state.rows!="") {

            //add data from excel
            let formData = new FormData();
            for(let i in this.state.rows) {
                let row = this.state.rows[i]
                if(!this.state.allGroceryNames.includes(row[0])){
                    formData.append("name", row[0])
                    formData.append("lat_0", row[1])
                    formData.append("lng_0", row[2])

                    //send to backend
                    await axios
                        .post(`/api/existingGrocery/`, formData)
                        .then(function (response) {
                            //handle success
                        })
                        .catch(function (response) {
                            //handle error
                        });
                }

            }
            toast.success("Excel wurde erfolgreich hochgeladen.")

        }
    }

    //put excel rows and columns into state
    fileHandler = (event) => {
        let fileObj = event.target.files[0];


        //just pass the fileObj as parameter
        ExcelRenderer(fileObj, (err, resp) => {
            //delete first row, the one with the column names
            resp.rows.shift()
            if(err){
                //handle error
            }
            else{
                this.setState({
                    cols: resp.cols,
                    rows: resp.rows
                });
            }
        });


    }

    render(){
        return(

            <div>
                <input accept=".xlsx, .xls, .csv" type="file" onChange={this.fileHandler.bind(this)} style={{"padding":"10px"}} />
                {/*{this.state.cols!=""?<OutTable data={this.state.rows} columns={this.state.cols} tableClassName="ExcelTable2007" tableHeaderRowClass="heading" />:""}*/}

                <button className={"button2"} onClick={this.onClickSend}>Sende das Excel</button>

                <button className={"button2"} >Lösche alle Einträge</button>
            </div>

        )
    }
}

export default ExistingServices;