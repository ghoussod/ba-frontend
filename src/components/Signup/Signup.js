import React, { Component } from "react";
import { withRouter } from "react-router-dom"; // new import
import { connect } from "react-redux"; // new import
import PropTypes from "prop-types"; // new import
import { Link } from "react-router-dom";
import {
    Container,
    Button,
    Row,
    Col,
    Form,
    FormControl
} from "react-bootstrap"; //We use bootstrap forms to create signup form. It is only a visual part of it.

import { signupNewUser } from "./SignupActions"; // new import

class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: ""
        };
    }
    onChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    // update function to call the action
    onSignupClick = () => {
        const userData = {
            username: this.state.username,
            password: this.state.password
        };
        this.props.signupNewUser(userData); // <-- signup new user request
    };

    render() {
        return (
            <div className={"background"}>
               <div className={"textArea2"} >
                <Row className={"shadow"}>
                    <Col>
                        <h1>Konto erstellen</h1>
                        <Form>
                            <Form.Group controlId="usernameId">
                                <Form.Label>Benutzername</Form.Label>
                                <Form.Control
                                    isInvalid={this.props.createUser.usernameError}
                                    type="text"
                                    name="username"
                                    placeholder="Geben Sie den Benutzernamen ein"
                                    value={this.state.username}
                                    onChange={this.onChange}
                                />
                                <FormControl.Feedback type="invalid">
                                    {this.props.createUser.usernameError}
                                </FormControl.Feedback>
                            </Form.Group>

                            <Form.Group controlId="passwordId">
                                <Form.Label>Ihr Passwort</Form.Label>
                                <Form.Control
                                    isInvalid={this.props.createUser.passwordError}
                                    type="password"
                                    name="password"
                                    placeholder="Geben Sie das Passwort ein"
                                    value={this.password}
                                    onChange={this.onChange}
                                />
                                <Form.Control.Feedback type="invalid">
                                    {this.props.createUser.passwordError}
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Form>
                        <Button color="primary" onClick={this.onSignupClick}>
                            Konto erstellen
                        </Button>
                        <p className="mt-2">
                            Haben Sie schon ein Konto? <Link to="/login">Login</Link>
                        </p>
                    </Col>
                </Row>
               </div>
            </div>
        );
    }
}

// connect action and reducer
// replace
// export default Signup;
// with code below:

Signup.propTypes = {
    signupNewUser: PropTypes.func.isRequired,
    createUser: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    createUser: state.createUser
});

export default connect(mapStateToProps, {
    signupNewUser
})(withRouter(Signup));