import React, {Component} from "react";
import axios from "axios";
import {GoogleApiWrapper, Map, Marker} from "google-maps-react";
import styles from './Evaluation.css';
import Select from 'react-select'
import ExistingServices from "./ExistingServices";
import {Link} from "react-router-dom";



class Evaluation extends Component{

    constructor(props) {
        super(props);
        this.state = {
            centers: [],
            //allMarker: [],
            nbOfCntrs: [],
            services: ["Lebensmittelgeschäft", "Recycling", "Post","Station"],
            allPoints: [],
            selectedService: 1, //1,2,3,4
            options: [
                { value: 5, label: '5 km' },
                { value: 4, label: '4 km' },
                { value: 3, label: '3 km' },
                { value: 2, label: '2 km' }
            ],
            selectedValue: 2,
        }
    }



    async componentDidMount() {
        // Get cluster centroids
        await axios
            .get(`clustering/clusters/`)
            .then((res) => {
                //localStorage.setItem('cluster', res.data);
                let array = [res.data.Grocery, res.data.Recycling, res.data.Post, res.data.Station]
                this.setState({centers: array})


            })
            .catch((error) => {
                console.log(error)
            })


        await axios
            .get(`clustering/allPoints/`)
            .then((res) => {
                this.setState({allPoints: res.data})

            })
            .catch((error) => {
                console.log(error)
            })



    }


    handleChangeSelect=(selectedOption)=>{
        this.setState({selectedValue: selectedOption.value})
    }



    //create 4 maps for each service with clusters
    createMaps(){
        let html = []
        let serviceIndex
        //for all services
        for(serviceIndex=0;serviceIndex<this.state.centers.length;serviceIndex++){

            //show only selected service
            if(serviceIndex==(this.state.selectedService-1)) {
                let nbCntrs = this.state.centers[serviceIndex][0].nbOfCntrs

                let allCentersOfService = this.state.centers[serviceIndex]
                nbCntrs = allCentersOfService.length - 1
                let markers = []

                while (nbCntrs != -1) {
                    markers.push(<Marker
                        position={{lat: allCentersOfService[nbCntrs].lat, lng: allCentersOfService[nbCntrs].lng}}/>)
                    nbCntrs--
                }
                html.push(
                    <div className={"four"}>
                        <h3>{/*this.state.services[serviceIndex]*/}Anzahl beste Orte=
                            {allCentersOfService[0].nbOfCntrs}</h3>
                        <Map
                            google={this.props.google}
                            style={{width: "30%", height: "50%"}}
                            className={""}
                            zoom={15}
                            //onClick={this.onClick}
                            initialCenter={
                                {
                                    lat: 46.80237,
                                    lng: 7.15128,
                                }
                            }
                        >{markers}</Map>
                        <br/> <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>


                    </div>)

            }
        }
        return (html)
    }

    //1 map for all datapoints of selected service
    showAll=()=>{
        let html = []
        let markers=[]
        let i
        let x = this.state.selectedService
        let additional = this.state.selectedService-1
        for(i=0;i<this.state.allPoints.length;i++){
            if(this.state.allPoints[i][x+additional]!=-100.000000){
                markers.push( <Marker position={{lat: this.state.allPoints[i][x+additional],lng:this.state.allPoints[i][x+additional+1]}}/>)}

        }
        html.push(<h3>Alle Vorschläge der Bürger</h3>)
        html.push(
            <Map
            google={this.props.google}
            style={{ width: "30%", height: "50%"}}
            className={""}
            zoom={15}
            //onClick={this.onClick}
            initialCenter={
                {
                    lat: 46.80237,
                    lng: 7.15128,
                }
            }
        >{markers}</Map>)
        return html
    }

    changeService =()=>{
        this.setState({selectedService: this.state.selectedService==4?1:this.state.selectedService+1})
    }


    render(){
        return(
            <div className={"evalbackground"}>
                <div className={"evaltextArea"}>
                    <div className={"text"}>
                        <h1>Evaluierung</h1>
                        <h2>Datenpunkte existierender Services hochladen</h2>
                        <ExistingServices></ExistingServices>
                    </div>
                    <hr className={"line"}/>

                    <h2>Alle Datenpunkte eines Service</h2>
                    <p className={"font1"}>In der ersten Karte sehen Sie alle Vorschläge der Bürger für eine Dienstleistung. Um die Dienstleistung zu wechseln, klicken Sie auf den Button. Die zweite Karte zeigt die empfohlenen Standorte für die ausgewählte Dienstleistung.</p>

                    <div className={"left"}>
                        <button className={"button3"} onClick={this.changeService}>
                            Klicke um den Service zu ändern: <b>{this.state.services[this.state.selectedService-1]}</b>
                        </button>
                        <div className={"space"}></div>

                        {this.showAll()}

                    </div>
                    <div className={"right"}>

                            <Link to={"/algorithm"}>
                                <button className={"button2 grey"}>
                                Mehr über den Algorithmus erfahren
                            </button>
                            </Link>

                        <div className={"space"}></div>

                        {this.createMaps()}
                    </div>

                </div>
        </div>
        )
    }
}

//GoogleApiWrapper to use google maps, apiKey to get full google maps
export default GoogleApiWrapper({
    apiKey:""
})(Evaluation);