import React, { Component } from "react";
import styles from "./Home.css"
import concept from "../concept.png";
import fpc from "../fpc.png"


class Algorithm extends Component {


    render() {
        return (
            <div className={"background"}>
                <div className={'textArea'}>
                    <h1>Informationen zum Algorithmus</h1>
                    <text className={"font3"}>Der verwendete Algorithmus ist der Fuzzy C-Means Algorithmus.
                        Mit diesem Algorithmus kann man Datenpunkte in Gruppen sortieren.
                        Das Besondere an diesem Algorithmus ist, dass jeder Datenpunkt einen Mitgliedschaftsgrad für jede Gruppe erhält.
                        Beim Vorgänger, dem K-Means Algorithmus, gehört jeder Datenpunkt nur einer Gruppe an.
                        Der Vorteil an der "Unschärfe bzw. Fuzziness" des Algorithmus ist, dass Datenpunkte, welche zwischen zwei Gruppen stehen, nicht gezwungen werden nur einer anzugehören. <br/>
                        In unserem Fall werden die Standortvorschläge gruppiert, um einen bestmöglichen Ort für eine Dienstleistung zu finden.
                        Jede Gruppe hat ein Zentrum, welches als der bestmögliche Ort gilt.
                        Um die Anzahl Gruppen zu bestimmen wird der Fuzzy Partition Coefficient verwendet.
                        Im Beispiel unten sieht man vier Standortvorschläge.
                        Der Algorithmus hat mit dem FPC berechnet, dass zwei Standorte das Optimum wären.
                        Je grösser der FPC, desto optimaler die Anzahl Gruppen.
                        In unserem Beispiel sieht man in der Mitte jedoch nur ein "bester" Ort.
                        Der Grund dafür ist, dass FPC erst bei einer Anzahl von zwei Gruppen verwendbar ist.
                        Bei einer Gruppe ergibt der FPC stets den optimalen Wert. Aus diesem Grund wird der FPC erst ab einer Anzahl von 2 Gruppen berechnet.
                        Um dieser Limite entgegenzuwirken, wird bei einem Optimum von zwei Gruppen die Distanz der beiden Standorte gemessen. Falls
                        sie näher als 100 Meter beieinander sind, wird nur ein Standort vorgeschlagen.
                    </text>
                    <img className={"image2"} src={fpc}/>

                </div>
            </div>
        );
    }
}

export default Algorithm;

