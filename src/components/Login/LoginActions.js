import axios from "axios";
import { push } from "connected-react-router";
import { toast } from "react-toastify";
import { SET_TOKEN, SET_CURRENT_USER, UNSET_CURRENT_USER } from "./LoginTypes";
import { setAxiosAuthToken, toastOnError } from "../../utils/Utils";
import Nav from "../Navbar/Nav";
//TODO: change toast msgs to german

//do a POST request to backend to get a token if username and password are correct
//set header with token
//get group of current user
export const login = (userData, redirectTo) => dispatch => {
    axios
        .post("/api/v1/token/login/", userData)
        .then(async response => {
            const {auth_token} = response.data;
            setAxiosAuthToken(auth_token);
            //dispatch is a function of the Redux store.
            // You call store.dispatch to dispatch an action.
            // This is the only way to trigger a state change.
            dispatch(setToken(auth_token));
            dispatch(getCurrentUser(redirectTo));
            toast.success("Login erfolgreich.");
            localStorage.setItem('loggedIn', true)
            axios.defaults.baseURL = 'http://127.0.0.1:8000/';

        })
        .catch(error => {
            dispatch(unsetCurrentUser());
            toastOnError(error);
        });
};

//do GET request to backend to get data of current user
export const getCurrentUser = redirectTo => dispatch => {
    axios
        .get("/api/v1/users/me/")
        .then(async response => {
            const user = {
                username: response.data.username,
                //email: response.data.email
            };

            const header = {
                headers: {
                    'Authorization': "Token " + localStorage.getItem('token')
                }
            }
            // Get the group of the user and save in localStorage
            await axios
                .get(`clustering/group/`, header)
                .then((res) => {
                    localStorage.setItem('group', res.data);
                    //toast.success('Ihre Gruppe ist ' + res.data + '!');
                    this.setState({group: res.data})

                })
                .catch((error) => {
                    console.log(error)
                })
            await dispatch(setCurrentUser(user, redirectTo));




        })
        .catch(error => {
            dispatch(unsetCurrentUser());
            toastOnError(error);
        });
};

//set the current user in localStorage
//if admin or expert refresh page so navbar is updated with evaluation item
export const setCurrentUser = (user, redirectTo) => dispatch => {
    localStorage.setItem("user", JSON.stringify(user));
    dispatch({
        type: SET_CURRENT_USER,
        payload: user
    });

    if (redirectTo !== "") {
        //redirect to map and load window again if navbar has to change, not best solution
        dispatch(push(redirectTo));

        if(localStorage.getItem("group")=="admin"||localStorage.getItem("group")=="expert"){
                window.location.assign(redirectTo)}
    }



};

//put token into storage
export const setToken = token => dispatch => {
    setAxiosAuthToken(token);
    localStorage.setItem("token", token);
    dispatch({
        type: SET_TOKEN,
        payload: token
    });
};

//remove token and user from storage
export const unsetCurrentUser = () => dispatch => {
    setAxiosAuthToken("");
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    dispatch({
        type: UNSET_CURRENT_USER
    });
};

//do logout with doing a POST request to backend
//set loggedIn to false and remove group of user in storage
export let logout = () => dispatch => {
    axios
        .post("/api/v1/token/logout/")
        .then(response => {
            dispatch(unsetCurrentUser());
            dispatch(push("/"));
            toast.success("Logout erfolgreich.");
            localStorage.setItem('loggedIn', false)
            localStorage.setItem('group', "");
        })
        .catch(error => {
            dispatch(unsetCurrentUser());
            toastOnError(error);
        });
};