import React, { Component } from "react";
import { withRouter } from "react-router-dom";  // new import
import { connect } from "react-redux";          // new import
import PropTypes from "prop-types";             // new import
import { Link } from "react-router-dom";
import { Container, Button, Row, Col, Form } from "react-bootstrap";

import { login } from "./LoginActions.js";
import axios from "axios";
import {toast} from "react-toastify";
import requireAuth from "../../utils/RequireAuth";      // new import

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: ""
        };
    }
    onChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    onLoginClick = async () => {

        const userData = {
            username: this.state.username,
            password: this.state.password
        };
        this.props.login(userData, "/home"); // <--- login request

        //add username to localstorage
        await localStorage.setItem("username", userData.username);

    };
    render() {
        return (
        <div className={"background"}>
            <div className={"textArea2"}>
                <Row className={"shadow"}>
                    <Col>
                        <h1>Login</h1>
                        <Form>
                            <Form.Group controlId="usernameId">
                                <Form.Label>Benutzername</Form.Label>
                                <Form.Control
                                    type="text"
                                    name="username"
                                    placeholder="Geben Sie den Benutzernamen ein"
                                    value={this.state.username}
                                    onChange={this.onChange}
                                />
                            </Form.Group>

                            <Form.Group controlId="passwordId">
                                <Form.Label>Ihr Passwort</Form.Label>
                                <Form.Control
                                    type="password"
                                    name="password"
                                    placeholder="Geben Sie das Passwort ein"
                                    value={this.state.password}
                                    onChange={this.onChange}
                                />
                            </Form.Group>
                        </Form>
                        <Button className={"primary"} onClick={this.onLoginClick}>
                            Login
                        </Button>
                        <p className="mt-2">
                            Haben Sie noch kein Konto? <Link to="/signup"> Konto erstellen</Link>
                        </p>
                    </Col>
                </Row>
            </div>
        </div>
        );
    }
}

// connect action and store and component
Login.propTypes = {
    login: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps, {
    login
})(withRouter(Login));