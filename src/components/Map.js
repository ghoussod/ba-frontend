import React, { Component } from "react";
import styles from './Map.css';


import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';
import axios from "axios";
import {toast} from "react-toastify";

class MapContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //array: index 0 grocery, index 1 recycling, index 2 post, index 3 station
            noNumb: -100.000000,
            markers: [
                {
                    //title: "grocery",
                    name: "grocery",
                    position: { lat: -100.000000, lng: -100.000000},
                    icon: "https://image.flaticon.com/icons/png/512/1261/1261163.png"
                },
                {
                    //title: "recycling",
                    name: "recycling",
                    position: { lat: -100.000000, lng: -100.000000 },
                    icon: "https://image.flaticon.com/icons/png/512/892/892930.png"
                },
                {
                    //title: "post",
                    name: "post",
                    position: { lat: -100.000000, lng: -100.000000 },
                    icon: "https://image.flaticon.com/icons/png/512/4616/4616153.png"
                },
                {
                    //title: "station",
                    name: "station",
                    position: { lat: -100.000000, lng: -100.000000 },
                    icon: "https://image.flaticon.com/icons/png/512/1042/1042263.png"
                }
            ],
            icon_index: 0,
            name: '',
            usernameNotExisting: true,
        };
        this.onClick = this.onClickMap.bind(this);
        //this.onClick2 = this.onClickIcon0.bind(this);
    }


    async componentDidMount() {
        await axios
            .get(`clustering/getOwnData/?name=${localStorage.getItem("username")}`)
            .then((res) => {
                if(res.data=="User Not Found"){
                }else{
                for(let index=0; index<4;index++){
                    //Copy all markers
                    let lat = parseFloat(res.data["lat_".concat(index)])
                    let lng = parseFloat(res.data["lng_".concat(index)])
                    let markers = [...this.state.markers];
                    //Make a copy of marker we want to mutate
                    let item = {...markers[index]};
                    //Change position and title of marker
                    if(lat==0||lng==0){
                        let noNumb= this.state.noNumb
                        item.position = {lat: noNumb, lng: noNumb}
                    }else{
                        item.position = {lat,lng};
                    }
                    //Put mutated marker back into array
                    markers[index] = item;
                    //Set the state to our new array of markers
                    this.setState({markers});

                }
                }

            })
            .catch((error) => {
                console.log(error)
            })
        //Set name to username
        this.setState(
            {
                name: localStorage.getItem("username")
            })
    }

    //Handler when clicking on the map
    onClickMap(t, map, coord) {

        //Get lat and lng of clicked point on map
        // longitude is horizontal, latitude is vertical
        const { latLng } = coord;
        const lat = latLng.lat();
        const lng = latLng.lng();

        //get index of current icon (service)
        const index = this.state.icon_index


        //Copy all markers
        let markers = [...this.state.markers];
        //Make a copy of marker we want to mutate
        let item = {...markers[index]};
        //Change position and title of marker
        item.position = {lat,lng};
        item.title = t;
        //Put mutated marker back into array
        markers[index] = item;
        //Set the state to our new array of markers
        this.setState({markers});


    }


    //Handler when clicking on icons
    onClickIcon=(e)=>{
        //change icon_index to the id of the current icon
        try {

            const id = e.target.id;
            this.setState({icon_index: parseInt(id)})

        }catch (error){console.log("Icon did not change. Error: ", error)}
    }

    //Handler for deleting one marker on the map
    onClickDelete=(e)=>{
        try {
            const id = e.target.id;

            //Make a copy of all markers
            let markers = [...this.state.markers];
            //Make a copy of marker we want to change
            let item = {...markers[id]};
            //Change lat and lng of marker
            item.position = {lat:this.state.noNumb,lng:this.state.noNumb};
            //Put mutated marker back into array
            markers[id] = item;
            //Set the state to the new array of markers
            this.setState({markers});

            console.log("Deletion successful.")
        }catch(error){
            console.log("Deletion not successful. Error: ",error)
        }
    }

    //Handler for deleting all markers on the map
    onClickDeleteAll=()=>{
        //-100 is value for not having a marker positioned
        let lat = this.state.noNumb
        let lng = this.state.noNumb
        //Copy of all markers
        let markers = [...this.state.markers];
        //for every marker
        for(let id=0;id<this.state.markers.length;id++){
            // Copy of marker
            let item = {...markers[id]};

            //Change position of marker
            item.position = {lat,lng};

            //Put marker back into array
            markers[id] = item;

        }
        //Set mutated markers to current state
        this.setState({markers});


    }


    //Handler when clicking on Send button
    onClickSend=async () => {
    //If username is already in db with markers, set usernameNotExisting to false else let it be true
        await axios
            .get(`api/db/`, {
            })
            .then((res) => {
                res.data.map(user => {
                        if (user.name === localStorage.getItem('username')){
                            this.setState({usernameNotExisting: false})
                        }
                    }
                )


            })
            .catch((error)=>{
                console.log(error)
            })



        //create FormData and add name and all lat/long of 4 services
        let formData = new FormData();
        let nbDigits = 6
        formData.append("name",this.state.name);
        formData.append("lat_0",this.state.markers[0].position.lat.toFixed(nbDigits));
        formData.append("lng_0",this.state.markers[0].position.lng.toFixed(nbDigits))
        formData.append("lat_1",this.state.markers[1].position.lat.toFixed(nbDigits) );
        formData.append("lng_1",this.state.markers[1].position.lng.toFixed(nbDigits))
        formData.append("lat_2",this.state.markers[2].position.lat.toFixed(nbDigits));
        formData.append("lng_2",this.state.markers[2].position.lng.toFixed(nbDigits))
        formData.append("lat_3",this.state.markers[3].position.lat.toFixed(nbDigits));
        formData.append("lng_3",this.state.markers[3].position.lng.toFixed(nbDigits))



        //post if user hasn't already sent marker positions
        if(this.state.usernameNotExisting){
            await axios
                .post(`/api/db/`, formData)
                .then(function (response) {
                    //handle success
                    toast.success("Orte wurden gesendet, danke!")
                })
                .catch(function (response) {
                    //handle error
                    console.log(response);
                    toast.success("Orte konnten nicht gesendet werden. Versuchen Sie erneut.")
                });
        }else{
            //put if user has already sent marker positions
            await axios
                .put(`/api/db/${localStorage.getItem('username')}/`, formData)
                .then(function (response) {
                    //handle success
                    toast.success("Orte wurden gesendet, danke!")
                })
                .catch(function (response) {
                    //handle error
                    console.log(response);
                    toast.success("Orte konnten nicht gesendet werden. Versuchen Sie erneut.")

                });
        }

        //this.onClickDeleteAll()

    }





    //check if there exists a marker on the map
    testAll(){
        let count = 0
        for(let id=0;id<this.state.markers.length;id++){
            if(this.state.markers[id].position.lat!=this.state.noNumb&&this.state.markers[id].position.lat!=NaN){
                if(count>0){
                    return true
                }
                count++
            }
        }
        return false

    }



    render() {
        return (
            <div className={"background"}>

                <h1 className={'textArea'}>Karte</h1><br/>

                <div className={'textArea'}>
                    <p className={"font1"}>Klicken Sie auf die Buttons um einen Service auszusuchen. Klicken Sie dann auf die Karte, wo Sie sich diesen Service wünschen würden. Jeder Service ist auf eine Markierung begrenzt. Klicken Sie anschliessend auf Senden um Ihre gewünschten Orte zu senden.<br/> Vielen Dank für die Partizipation!</p><br/>
                </div>

                <div className={"title-icons"}>
                    <div className={"inLine"}>
                        <div className={"icons"}>
                            <button className={"button"} id={0} onClick={this.onClickIcon}>
                                <img id={0} className={"image"} src={this.state.markers[0].icon} onClick={this.onClickIcon}/>
                                Lebensmittelgeschäft
                            </button>
                        </div>
                        <div className={"deletions"}>
                            {this.state.markers[0].position.lat !=this.state.noNumb&&this.state.markers[0].position.lat!=NaN?
                                <button className={"button red"} id={0} onClick={this.onClickDelete}>Löschen</button> : ""}
                        </div>
                    </div>
                    <div className={"inLine"}>
                        <div className={"icons"}>
                            <button className={"button"} id={1} onClick={this.onClickIcon}>
                                <img id={1} className={"image"} src={this.state.markers[1].icon} onClick={this.onClickIcon}/>
                                Recycling
                            </button>
                        </div>
                        <div className={"deletions"}>
                            {this.state.markers[1].position.lat != this.state.noNumb ?
                                <button className={"button red"} id={1} onClick={this.onClickDelete}>Löschen</button> : ""}
                        </div>
                    </div>
                    <div className={"inLine"}>
                        <div className={"icons"}>
                            <button className={"button"} id={2} onClick={this.onClickIcon}>
                                <img id={2} className={"image"} src={this.state.markers[2].icon} onClick={this.onClickIcon}/>
                                Post
                            </button>
                        </div>
                        <div className={"deletions"}>
                            {this.state.markers[2].position.lat != this.state.noNumb?
                                <button className={"button red"} id={2} onClick={this.onClickDelete}>Löschen</button> : ""}
                        </div>
                    </div>
                    <div className={"inLine"}>
                        <div className={"icons"}>
                            <button className={"button"} id={3} onClick={this.onClickIcon}>
                                <img id={3} className={"image"} src={this.state.markers[3].icon} onClick={this.onClickIcon}/>
                                 Station
                            </button>
                        </div>
                        <div className={"deletions"}>
                            {this.state.markers[3].position.lat != this.state.noNumb ?
                                <button className={"button red"} id={3} onClick={this.onClickDelete}>Löschen</button> : ""}
                        </div>
                    </div>
                    <div className={"inLine"}>
                        <div className={"icons"}>
                            <button className={"button"} onClick={this.onClickSend}>Senden</button>
                        </div>
                        <div className={"deletions"}>
                            {this.testAll() ? <button className={"button red"} onClick={this.onClickDeleteAll}>Alle löschen</button> : ""}
                        </div>
                    </div>
                </div>

                <div className={"map"}>
                    <Map
                        google={this.props.google}
                        style={{ width: "100%", height: "60%"}}
                        className={"map2"}
                        zoom={15}
                        onClick={this.onClick}
                        initialCenter={
                            {
                                lat: 46.80237,
                                lng: 7.15128,
                            }
                        }
                    >
                        {this.state.markers.map((marker, index) => (
                            marker.position.lat!=this.state.noNumb?<Marker
                                key={index}
                                title={marker.title}
                                name={marker.name}
                                position={marker.position}
                                icon={{

                                    url: marker.icon,
                                    scaledSize: new window.google.maps.Size(30, 30)
                                }}
                            />:""
                        ))}
                    </Map>

                </div>


                <div className={"iconsRef"}>
                    <div>Icons made by <a href="https://www.flaticon.com/authors/monkik" title="monkik">monkik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
                    <div>Icons made by <a href="https://www.flaticon.com/authors/pixel-perfect" title="Pixel perfect">Pixel perfect</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
                    <div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
                    <div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
                </div>

            </div>

        );
    }
}


export default GoogleApiWrapper({
    //uncomment to have a real google map
    apiKey: ""
})(MapContainer);