
import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

// import new reducer
import { signupReducer } from "./components/Signup/SignupReducer";
import { loginReducer } from "./components/Login/LoginReducer";

//If we will create a new reducer for a component we will add it here.
// It is the main reducer that concatenates all component’s reducers from the application.
const createRootReducer = history =>
    combineReducers({
        router: connectRouter(history),
        createUser: signupReducer,
        auth: loginReducer
    });

export default createRootReducer;