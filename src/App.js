import React, {Component} from "react";

import axios from "axios";

//all components
import MapContainer from "./components/Map";
import Login from "./components/Login/Login";
import Signup from "./components/Signup/Signup";
import Home from "./components/Home";
import Evaluation from "./components/Evaluation";

import style from "./App.css"

import {toast, ToastContainer} from "react-toastify";

//Navigation bar
import Nav from "./components/Navbar/Nav";

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { Route, Switch} from "react-router-dom";

//replace BrowserRouter with Root
import Root from "./Root";

//used to check if user is logged in (authenticated)
import requireAuth from "./utils/RequireAuth";

//used to redirect from one route to another
import {Redirect} from "react-router";
import ExistingServices from "./components/ExistingServices";
import requirePermission from "./utils/RequirePermission";
import Algorithm from "./components/Algorithm";

//URL to access backend
axios.defaults.baseURL = "http://127.0.0.1:8000";



class App extends Component {
  constructor(props) {
    super(props);
  }



  render() {

    // Navigation bar
    // Different routes:
    // home and map only accessible if logged in
    // evaluation only accessible if logged in and admin
    // ToastContainer to show toasts
    return (
        <div className={"App"}>
          <Root>
            <Nav />
            <Switch>
              {/*"exact path" would also be possible or to use * as path -> default match*/}
              <Route path="/home" component={Home}/>
              <Route path="/algorithm" component={Algorithm}/>

              <Route path="/signup" component={Signup}/>
              <Route path="/login" component={Login}/>
              <Route path="/map" component={requireAuth(MapContainer)}/>
              <Route path="/evaluation" component={requirePermission(Evaluation)}/>
              {/*<Route path="/upload" component={requireAuth(ExistingServices)}/>*/}
              <Route path="/" ><Redirect to={"/home"}/></Route>
            </Switch>
          </Root>
          <ToastContainer hideProgressBar={false} newestOnTop={true} />
      </div>
    );
  }
}

export default App;