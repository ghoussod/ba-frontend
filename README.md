#Installation and How To Run

Prerequisites
```sh
npm
```

Clone the repository
```sh
$ git clone https://gitlab.com/ghoussod/ba-frontend.git
$ cd ba-frontend
```

Install dependencies
```sh
$ npm install
```

Start application
```sh
$ npm start
```

And navigate to `http://localhost:3000/`.

Login with

For Expert Rights- Username: Expert PW: bachelor2021

For Admin Rights- Username: Admin PW: bachelor2021

For User Rights- create a new user via "Konto erstellen"
